#!/bin/bash 

read n
total=0
for((i=0;i<n;i++))
do
read x
total=$((total + x))
done
printf "%.3f\n" $(echo "scale = 4; $total / $n " | bc )

