#!/bin/bash

i=0
while read country
do
    array[i]=$country
    i=$((i+1))
done

declare -a new_array=( ${array[@]/*[aA]*/} )
echo "${new_array[@]}"
